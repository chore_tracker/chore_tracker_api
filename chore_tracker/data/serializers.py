from marshmallow import Schema

class FamilySerializer(Schema):
    class Meta():
        fields = ('family_id', 'family_name',)

class UserSerializer(Schema):
    class Meta():
        fields = ('id', 'family_id','name', 'username', 'role')