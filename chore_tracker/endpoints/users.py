from uuid import uuid4

from flask import Blueprint, request
from flask_login import login_required
from werkzeug.security import generate_password_hash

from chore_tracker.helpers import prefix
from chore_tracker.data.models import User, Family, db
from chore_tracker.data.serializers import UserSerializer


users_view = Blueprint('users', __name__)

@users_view.route(prefix('/users'), methods=('POST',))
def create_user():
    '''
    Sample Body:
    {
        name, //string: required
        username, //string: required
        password, //string: required
        role, //string: required
        family_id, //uuid: optional
        family_name, //string: optional
    }
    '''
    existing_user = User.query.filter_by(username=request.json['username']).first()
    # return an error if the username already exists
    if existing_user:
        return '', 409
    
    # create a new user
    new_user = User(
        username=request.json['username'],
        name=request.json['name'],
        password=generate_password_hash(request.json['password']),
        role=request.json['role']
    )

    # a family_name will be passed if the user chooses to create a family
    family_name = request.json.get('family_name', None)
    if family_name:
        # generate family_id
        family_id = uuid4()
        family_exists = Family.query.filter_by(family_id=family_id).first()

        # it is unlikely than an existing code is generated but we still check just in case
        while family_exists:
            family_id = uuid4()
            family_exists = User.query.filter_by(family_id=family_id).first()

        # create new family
        new_family = Family(family_name=family_name, family_id=family_id)

        db.session.add(new_family)
        
        new_user.family_id = family_id

    # a family_id will be passed if the user wants to join an existing family
    family_id = request.json.get('family_id', None)
    if family_id:
        try:
            family_exists = Family.query.filter_by(family_id=family_id).first()

            if not family_exists:
                raise Exception
        except:
            return 'Invalid family_id', 400
        
        new_user.family_id = family_id
    
    db.session.add(new_user)
    db.session.commit()

    return { 'family_id': new_user.family_id }, 201

@users_view.route(prefix('/users/<username>'), methods=('GET','PUT',))
@login_required
def get_update_user(username):
    query = User.query.filter_by(username=username)
    user = query.first()

    if not user:
        return 'User Not Found', 404
    
    if request.method == 'GET':
        serializer = UserSerializer()
        return serializer.dump(user)
    elif request.method == 'PUT':
        '''
        Sample Body:
        {
            name, // string: optional
            family_id, // uuid: optional
            password, // string: optional
            role, // string: optional
        }
        '''
        if request.json.get('username', None):
            del request.json['username']

        if request.json.get('password', None):
            request.json['password'] = generate_password_hash(request.json['password'])

        try:
            query.update(request.json)
        except:
            return 'Bad Request Body', 400
        db.session.commit()
        
        return '', 204