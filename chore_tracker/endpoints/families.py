from uuid import uuid4

from flask import Blueprint, request
from flask_login import login_required

from chore_tracker.helpers import prefix
from chore_tracker.data.models import User, Family, db
from chore_tracker.data.serializers import UserSerializer, FamilySerializer


families_view = Blueprint('families', __name__)

@families_view.route(prefix('/families'), methods=('GET',))
def get_family_count():
  '''
  At the time of writing this code I cannot think of a reason to return all 
  the family objects and if we did it would probably need pagination. Until
  a use case for returning all family records arises this will just return 
  the count of records in the table.
  '''
  family_count = Family.query.count()

  return { 'count': family_count }, 200

@families_view.route(prefix('/families/<family_id>'), methods=('GET', 'PUT'))
@login_required
def get_or_update_family(family_id):
  try:
    family = Family.query.filter_by(family_id=family_id).first()
    
    if not family:
      raise Exception
  except:
    return 'Invalid family_id', 400
  
  if request.method == 'GET':
    family_dict = FamilySerializer().dump(family)
    family_members = []
    user_serializer = UserSerializer()

    for family_member in User.query.filter_by():
      family_members.append(user_serializer.dump(family_member))

    family_dict['family_members'] = family_members

    return family_dict, 200
  
  if request.method == 'PUT':
    new_family_name = request.json.get('family_name', None)

    if not new_family_name:
      return 'You must provide a family name', 400
    
    family.family_name = new_family_name

    db.session.commit()

    return '', 204

