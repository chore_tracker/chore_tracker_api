from flask import Blueprint, request

from chore_tracker.helpers import prefix

health_view = Blueprint('health', __name__)

@health_view.route(prefix('/health'), methods=('GET',))
def get_family_count():
  return '', 200