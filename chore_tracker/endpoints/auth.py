from flask import Blueprint, request
from flask_login import login_user, logout_user, login_required, current_user
from werkzeug.security import check_password_hash

from chore_tracker.helpers import prefix
from chore_tracker.data.models import User

auth_view = Blueprint('auth', __name__)

@auth_view.route(prefix('/login'), methods=('POST',))
def login():
    user = User.query.filter_by(username=request.json['username']).first()

    if not user or not check_password_hash(user.password, request.json['password']):
        return { 'error': 'Invalid username or password' }, 401
    
    login_user(user, remember=True)
    return ''

@auth_view.route(prefix('/logout'), methods=('POST',))
@login_required
def logout():
    logout_user()
    return ''