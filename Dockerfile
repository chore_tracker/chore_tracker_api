FROM python
RUN mkdir /chore_tracker
COPY chore_tracker /chore_tracker
COPY requirements.txt .
COPY .env .
RUN pip install -r requirements.txt
ENTRYPOINT ["gunicorn", "--bind", "0.0.0.0:80", "chore_tracker:create_app()"]