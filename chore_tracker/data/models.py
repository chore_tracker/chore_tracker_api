from uuid import uuid4

from flask_login import UserMixin
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import UUID

db = SQLAlchemy()
migrate = Migrate()

class Family(db.Model):
    __tablename__ = 'families'

    family_id = db.Column(UUID(as_uuid=True), primary_key=True)
    family_name = db.Column(db.String(), nullable=False)

class User(UserMixin, db.Model):
    __tablename__ = 'family_members'

    id = db.Column(db.Integer(), primary_key=True)
    family_id = db.Column(UUID(as_uuid=True), db.ForeignKey('families.family_id'), nullable=True)
    name = db.Column(db.String(), nullable=False)
    username = db.Column(db.String(), unique=True, nullable=False)
    password = db.Column(db.String(), nullable=False)
    role = db.Column(db.String(), nullable=False)
    