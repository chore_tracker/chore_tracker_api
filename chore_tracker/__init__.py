# each import group should impor in alphabetical order with

# import base python packages first
import os

# import pypi packages next
from dotenv import load_dotenv
from flask import Flask
from flask_login import LoginManager
from sqlalchemy import URL

# import local pacakges last
from chore_tracker.endpoints.auth import auth_view
from chore_tracker.endpoints.families import families_view
from chore_tracker.endpoints.health import health_view
from chore_tracker.endpoints.users import users_view
from chore_tracker.data.models import db, migrate, User

load_dotenv()

def create_app():
    app = Flask(__name__)
    database_url = URL.create(
        drivername='postgresql',
        username=os.getenv('DB_USER'),
        password=os.getenv('DB_PASSWORD'),
        host=os.getenv('DB_HOST'),
        port=os.getenv('DB_PORT'),
        database=os.getenv('DB'),
    )
    app.config['SQLALCHEMY_DATABASE_URI'] = database_url
    app.config['SECRET_KEY'] = os.getenv('SECRET_KEY')

    login_manager = LoginManager()

    # initialize extensions in alphabetical order for ease of maintenance
    db.init_app(app)
    login_manager.init_app(app)
    migrate.init_app(app, db)

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    # register the views in alphabetical order for ease of maintenance
    app.register_blueprint(auth_view)
    app.register_blueprint(users_view)
    app.register_blueprint(families_view)
    app.register_blueprint(health_view)
    
    return app